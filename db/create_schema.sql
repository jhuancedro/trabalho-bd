DROP SCHEMA suspeitos CASCADE;
CREATE SCHEMA suspeitos;

SET SEARCH_PATH = postgres, suspeitos;

CREATE TABLE local (
    id          INTEGER PRIMARY KEY,
    descricao   VARCHAR,
    estado      VARCHAR(20),
    cidade      VARCHAR(30),
    bairro      VARCHAR(30),
    rua         VARCHAR(50),
    numero      INTEGER,
    complemento VARCHAR(10)
);

CREATE TABLE departamento_policia (
    numero VARCHAR(10) PRIMARY KEY,
    tipo   VARCHAR(20),
    local  INTEGER REFERENCES local
);

CREATE TABLE tipo_ocorrencia (
    nome      VARCHAR(15) PRIMARY KEY,
    descricao VARCHAR
);

CREATE TABLE ocorrencia (
    registro  VARCHAR(10) PRIMARY KEY,
    descricao VARCHAR,
    data      TIMESTAMP,
    local     INTEGER REFERENCES local,
    dp        VARCHAR(10) REFERENCES departamento_policia
);

-- TODO: todos os individuos devem ser vitima ou suspeito
CREATE TABLE individuo (
    id        INTEGER PRIMARY KEY,
    nome      VARCHAR(50),
    rg        VARCHAR(15) UNIQUE,
    cpf       VARCHAR(11) UNIQUE,
    idade     INTEGER,
    descricao VARCHAR
);

CREATE TABLE vitima (
    individuo INTEGER REFERENCES individuo PRIMARY KEY
);

CREATE TABLE suspeito (
    individuo INTEGER REFERENCES individuo PRIMARY KEY,
    alcunha   VARCHAR(20)
);

CREATE TABLE participacao_individuo_ocorrencia (
    individuo  INTEGER REFERENCES individuo,
    ocorrencia VARCHAR(10) REFERENCES ocorrencia,
    descricao  VARCHAR,

    PRIMARY KEY (individuo, ocorrencia)
);

CREATE TABLE tipo_caracteristica (
    nome      VARCHAR(15) PRIMARY KEY,
    descricao VARCHAR
);

CREATE TABLE reconhecimento_caracteristica (
    suspeito       INTEGER REFERENCES suspeito,
    caracteristica VARCHAR(15) REFERENCES tipo_caracteristica,
    descricao      VARCHAR,
    PRIMARY KEY (suspeito, caracteristica)
);

CREATE TABLE organizacao_criminosa (
    id             INTEGER PRIMARY KEY,
    nome           VARCHAR(30),
    descricao      VARCHAR,
    numero_membros INTEGER
);

CREATE TABLE vinculo_criminoso (
    individuo   INTEGER REFERENCES individuo,
    organizacao INTEGER REFERENCES organizacao_criminosa,
    PRIMARY KEY (individuo, organizacao)
);

CREATE TABLE participacao_organizacao_ocorrencia (
    organizacao INTEGER REFERENCES organizacao_criminosa,
    ocorrencia  VARCHAR(10) REFERENCES ocorrencia,
    PRIMARY KEY (organizacao, ocorrencia)
);

CREATE TABLE agente (
    codigo VARCHAR(10) PRIMARY KEY,
    nome   VARCHAR(50),
    cpf    VARCHAR(11),
    rg     VARCHAR(15),
    cargo  VARCHAR(20),
    dp     VARCHAR(10) REFERENCES departamento_policia
);

CREATE TABLE relato_ocorrencia_agente (
    ocorrencia VARCHAR(10) REFERENCES ocorrencia,
    agente     VARCHAR(10) REFERENCES agente,
    PRIMARY KEY (ocorrencia, agente)
);

CREATE TABLE presidio (
    numero VARCHAR(10) PRIMARY KEY,
    nome   VARCHAR(15),
    tipo   VARCHAR(20),
    local  INTEGER REFERENCES local
);

-- TODO: dtaa do inquerito deve ser superior aa da ocorrencia
CREATE TABLE inquerito (
    numero       VARCHAR(10) PRIMARY KEY,
    descricao    VARCHAR,
    data_inicio  TIMESTAMP,
    data_termino TIMESTAMP,
    ocorrencia   VARCHAR(10) REFERENCES ocorrencia
);

CREATE TABLE instauracao_inquerito (
    ocorrencia VARCHAR(10) REFERENCES ocorrencia,
    inquerito  VARCHAR(10) REFERENCES inquerito,
    PRIMARY KEY (ocorrencia, inquerito)
);

CREATE TABLE investigador_inquerito (
    agente    VARCHAR(10) REFERENCES agente,
    inquerito VARCHAR(10) REFERENCES inquerito,
    PRIMARY KEY (agente, inquerito)
);

CREATE TABLE acao_penal (
    numero       VARCHAR(10) PRIMARY KEY,
    descricao    VARCHAR,
    data_inicio  DATE,
    data_termino DATE,
    inquerito    VARCHAR(10) REFERENCES inquerito,
    presidio     VARCHAR(10) REFERENCES presidio,
    reu          INTEGER REFERENCES suspeito
);

CREATE TABLE relato_ocorrencia_vitima (
    vitima     INTEGER REFERENCES vitima,
    ocorrencia VARCHAR(10) REFERENCES ocorrencia,
    data       TIMESTAMP,

    PRIMARY KEY (vitima, ocorrencia)
);

CREATE TABLE tipo_objeto (
    nome      VARCHAR(30) PRIMARY KEY,
    descricao VARCHAR
);

CREATE TABLE objeto (
    id        INTEGER PRIMARY KEY,
    nome      VARCHAR(20),
    tipo      VARCHAR(30) REFERENCES tipo_objeto,
    descricao VARCHAR
);

CREATE TABLE ligacao_ocorrencia_objeto (
    objeto     INTEGER REFERENCES objeto,
    ocorrencia VARCHAR(10) REFERENCES ocorrencia,
    tipo       VARCHAR(30),
    PRIMARY KEY (objeto, ocorrencia)
);

CREATE TABLE relacao_tipo_ocorrencia (
    tipo       VARCHAR(15) REFERENCES tipo_ocorrencia,
    ocorrencia VARCHAR(10) REFERENCES ocorrencia,

    PRIMARY KEY (tipo, ocorrencia)
);