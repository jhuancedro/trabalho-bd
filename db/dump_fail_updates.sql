-- Tentativa de Inserção de documento repetido de um indivíduo.
INSERT INTO individuo (id, nome, rg, cpf, idade, descricao) VALUES (
    4,
    'Tomé',
    '2796250024',
    '12345678900',
    14,
    'Sem 3 dentes da frente, não acredita nas coisas'
);
SELECT * FROM individuo;
INSERT INTO individuo (id, nome, rg, cpf, idade, descricao) VALUES (
    5,
    'Maria Madalena',
    'mg27450313',
    '14785236955',
    14,
    'Veste roupas insinuosas'
);

-- Tentativa de remoção de um Local
DELETE FROM local
WHERE bairro = 'Sao Pedro';

-- Tentativa de atualização de Chave Primária
UPDATE local
SET id = 10
WHERE cidade = 'Juiz de Fora';