SET SEARCH_PATH = postgres, suspeitos;

-- A cada novo suspeito de uma organização, aumenta-se o número de membros dela
CREATE FUNCTION aumenta_org()
    RETURNS TRIGGER AS
'BEGIN
    UPDATE organizacao_criminosa
    SET numero_membros = numero_membros + 1
    WHERE
        organizacao_criminosa.id = new.organizacao;
    RETURN new;
END;'
LANGUAGE plpgsql;

CREATE TRIGGER aumenta_org
AFTER INSERT ON vinculo_criminoso
FOR EACH ROW
EXECUTE PROCEDURE aumenta_org();

-- A cada nova acao penal, uma organização diminui o número de membros dela
CREATE FUNCTION diminui_org()
    RETURNS TRIGGER AS
'BEGIN
    UPDATE organizacao_criminosa AS oc
    SET numero_membros = numero_membros - 1
    WHERE
        oc.id = vinculo_criminoso.organizacao AND
        vinculo_criminoso.individuo = new.reu;
    RETURN new;
END;'
LANGUAGE plpgsql;

CREATE TRIGGER diminui_organizacao
AFTER INSERT ON acao_penal
FOR EACH ROW
EXECUTE PROCEDURE diminui_org();