SET SEARCH_PATH = postgres, suspeitos;

-- Consulta 1: Listar todos suspeitos: alcunha, nome e cpf, ordenados por alcunha
SELECT alcunha, nome, cpf
FROM suspeito AS s, individuo AS i
WHERE i.id = s.individuo
ORDER BY alcunha;

-- Consulta 2: Listar todas ocorrências do Bairro São Pedro
SELECT *
FROM ocorrencia
WHERE ocorrencia.local IN
      (SELECT id
       FROM local
       WHERE bairro = 'Sao Pedro');

-- Consulta 3: Listar todas ocorrências dos últimos 30 dias
SELECT *
FROM ocorrencia
WHERE ocorrencia.data >= CURRENT_DATE - INTERVAL '30 days';

-- Consulta 4: Listar local, horário e idade das vítimas de ocorrências
SELECT idade, CAST(data AS TIME) AS horario, local
FROM
    -- Conculta data e local das ocorrencias
    (SELECT registro, local, data
     FROM ocorrencia) AS ocr,

    -- Consulta vitimas que participaram de ocorrencia
    (SELECT ind_v.individuo, ocorrencia
     FROM
         participacao_individuo_ocorrencia AS pio,

         -- Consulta individuos que sao vitimas
         (SELECT individuo
          FROM individuo, vitima
          WHERE individuo.id = vitima.individuo) AS ind_v
     WHERE pio.individuo = ind_v.individuo) AS vtm_o,

    -- Consulta idade dos individuos
    (SELECT id, idade
     FROM individuo) AS ind
WHERE ocr.registro = vtm_o.ocorrencia AND ind.id = vtm_o.individuo;

-- Consulta 5: Listar todos presídios e seus locais
SELECT nome, tipo, cidade, estado, bairro, rua, local.numero, complemento
FROM
  presidio INNER JOIN local
    ON presidio.local = local.id;

-- Consulta 6: Listar todos membros de organização criminosa “X”
SELECT id, nome, rg, cpf, idade, descricao
FROM
    -- Consulda individuos de uma organizacao criminosa
    (SELECT individuo AS id
     FROM
         organizacao_criminosa AS oc,
         vinculo_criminoso AS vc
     WHERE oc.id = vc.organizacao AND oc.nome = 'Gangue dos bandidos') AS ioc
    NATURAL JOIN
    -- Consulta individuos
    individuo;

-- Consulta 7: Horário de ocorrências com relação a organização criminosa “X”
SELECT CAST(data AS TIME) AS "Horario das ocorrencias da Gangue dos bandidos"
FROM
    -- Consulta organizacoes criminosas
    (SELECT id, nome
     FROM organizacao_criminosa
     WHERE nome = 'Gangue dos bandidos') AS oc,

    -- Consulta participacoes
    (SELECT poo.organizacao, oc.data
     FROM
         participacao_organizacao_ocorrencia AS poo,
         ocorrencia AS oc
     WHERE poo.ocorrencia = oc.registro) AS p

WHERE oc.id = p.organizacao;

-- Consulta 8: Listar todas vítimas de ao menos uma ocorrência
SELECT id, nome, rg, cpf, idade, descricao
FROM
    individuo,

    -- Consulta individuos vitimas de ocorrencias
    (SELECT pio.individuo
     FROM
         participacao_individuo_ocorrencia AS pio,
         ocorrencia AS o,
         vitima AS v
     WHERE
         pio.ocorrencia = o.registro AND
         v.individuo = pio.individuo) AS vo
WHERE individuo.id = vo.individuo;

-- Consulta 9: Quais suspeitos tem a característica do tipo “X” e “Y”
SELECT *
FROM individuo
WHERE individuo.id IN
      (SELECT *
       FROM
           -- Consulta suspeitos que possuem tatuagem
           (SELECT suspeito
            FROM reconhecimento_caracteristica
            WHERE caracteristica = 'tatuagem') AS st
       INTERSECT
       -- Consulta suspeitos que sao aleijados
       (SELECT suspeito
        FROM reconhecimento_caracteristica
        WHERE caracteristica = 'aleijamento')
      );

-- Consulta 10: Qual departamento registrou ocorrências hoje
SELECT departamento_policia.numero
FROM departamento_policia, ocorrencia
WHERE
    ocorrencia.dp = departamento_policia.numero AND
    CAST(ocorrencia.data AS DATE) = current_date;

-- Consulta 11: Listar veículos do modelo “X”, cor preta
SELECT *
FROM objeto
WHERE
    tipo = 'Automovel' AND
    descricao ILIKE '%pret%'
    AND descricao ILIKE '%Siena%';

-- Consulta 12: Quais inquéritos não resultaram em ação penal?
SELECT *
FROM
    (SELECT numero FROM inquerito
     EXCEPT
     (SELECT inquerito AS numero FROM acao_penal)
    ) AS num_inq
    NATURAL JOIN
    inquerito;

-- Consulta 13: Quais bairros e cidades possuem presídio ou departamento de polícia?
SELECT bairro, cidade, estado
FROM
    (SELECT local FROM presidio
     UNION
     SELECT local FROM departamento_policia) AS union_locais,
    local
WHERE union_locais.local = local.id;

-- Consulta 14: Quais agentes do departamento de polícia X são investigadores no inquérito Y?
SELECT codigo, nome, cargo, cpf, rg
FROM agente
WHERE
    dp = 'PM 14B' AND
    codigo IN
    -- Agentes investigadores de um inquerito
    (SELECT agente
     FROM investigador_inquerito
     WHERE inquerito = '0001');

-- Consulta 15: Listar todas vítimas de roubo de celular
SELECT individuo, nome, rg, cpf, idade
FROM
    -- Consulta ocorrencias de roubo de celular
    (SELECT ocorrencia
     FROM
         -- Conculta objetos que sao celulares
         (SELECT id
          FROM objeto
          WHERE nome = 'Celular') AS cel,

         -- Consulta ligacao entre objeto e ocorrencia
         (SELECT objeto, ocorrencia
          FROM ligacao_ocorrencia_objeto) AS obj
     WHERE cel.id = obj.objeto) AS ocorr_cel,

    -- Consulta vitimas que participaram de ocorrencia
    (SELECT ind_v.individuo, nome, rg, cpf, idade, ocorrencia
     FROM
         participacao_individuo_ocorrencia AS pio,

         -- Consulta individuos que sao vitimas
         (SELECT individuo, nome, rg, cpf, idade
          FROM individuo, vitima
          WHERE individuo.id = vitima.individuo) AS ind_v
     WHERE pio.individuo = ind_v.individuo) AS vitm_ocorr;

-- Consulta 16: Listar as cidades com mais de 5 ocorrências, ordenado pela quantidade de ocorrências.

SELECT l.cidade, COUNT(*) AS numero_de_ocorrencias
FROM
    local l
    INNER JOIN ocorrencia o ON (l.id = o.local)
GROUP BY
    l.cidade
HAVING
    COUNT(*) > 5
ORDER BY
    numero_de_ocorrencias;

-- Consulta 17: Listar as cidades que não possuem ocorrências
SELECT l.cidade AS cidades_seguras
FROM
    local l
    LEFT OUTER JOIN suspeitos.ocorrencia o ON (l.id = o.local)
WHERE
    o.registro IS NULL;