SET SEARCH_PATH = postgres, suspeitos;

-- Tres locais
INSERT INTO local (id, descricao, estado, cidade, bairro, rua, numero, complemento) VALUES (
    1,
    'Um local bonito e seguro',
    'Minas Gerais',
    'Juiz de Fora',
    'Sao Pedro',
    'Jose Lourenco Kelmer',
    257,
    NULL
);
INSERT INTO local (id, descricao, estado, cidade, bairro, rua, numero, complemento) VALUES (
    2,
    'Um local bonito e perigoso',
    'Rio de Janeiro',
    'Matias Barbosa',
    'Alto do Fundao',
    'Canavial de emocoes',
    171,
    'Casa B'
);
INSERT INTO local (id, descricao, estado, cidade, bairro, rua, numero, complemento) VALUES (
    3,
    'Um local distante e quieto',
    'Rio de Janeiro',
    'Volta Redonda',
    'Recanto do Sol',
    'Jose aldo',
    NULL,
    NULL
);

-- Dois departamentos de policia
INSERT INTO departamento_policia (numero, tipo, local) VALUES ('PM 14B', 'Militar', 1);
INSERT INTO departamento_policia (numero, tipo, local) VALUES ('PCMG 28AF', 'Civil', 2);

-- Tipos de ocorrencia
INSERT INTO tipo_ocorrencia (nome, descricao) VALUES (
    'Homicidio',
    'Destruição, voluntária ou involutária, da vida de um ser humano; assassínio, assassinato.'
);
INSERT INTO tipo_ocorrencia (nome, descricao) VALUES (
    'Roubo',
    'Apropriação indébita de bem alheio por meio de violência ou de grave ameaça.'
);
INSERT INTO tipo_ocorrencia (nome, descricao) VALUES (
    'Estupro',
    'Crime que consiste no constrangimento a relações sexuais por meio de violência; violação.'
);
INSERT INTO tipo_ocorrencia (nome, descricao) VALUES (
    'Latrocinio',
    'Homicídio com objetivo de roubo, ou roubo seguido de morte ou de graves lesões corporais da vítima.'
);

-- Ocorrencias
INSERT INTO ocorrencia (registro, descricao, data, local, dp) VALUES (
    '26489213',
    'A vitima saia do supermercado quando foi surpreenida pelo bandido que portava um revolver calibre 001.' ||
    'Sua bolsa foi levada com seus pertences, incluindo...',
    DATE '2016-09-28' + TIME '15:30',
    1,
    'PM 14B'
);
INSERT INTO ocorrencia (registro, descricao, data, local, dp) VALUES (
    '26172839',
    'A vitima foi assassinada a sangue frio de maneira desonhosa',
    DATE '2016-12-21' + TIME '23:12',
    2,
    'PCMG 28AF'
);

-- relacao da ocorrencia com o seu tipo
INSERT INTO relacao_tipo_ocorrencia (tipo, ocorrencia) VALUES ('Roubo', '26489213');
INSERT INTO relacao_tipo_ocorrencia (tipo, ocorrencia) VALUES ('Homicidio', '26172839');

-- Individuos
INSERT INTO individuo (id, nome, rg, cpf, idade, descricao) VALUES (
    1,
    'Pedro',
    'mg27450313',
    '12345678900',
    34,
    'Foi assaltado'
);
INSERT INTO individuo (id, nome, rg, cpf, idade, descricao) VALUES (
    2,
    'Jose',
    'mg36987211',
    NULL,
    42,
    'Testemunha de um crime'
);
INSERT INTO individuo (id, nome, rg, cpf, idade, descricao) VALUES (
    3,
    'Judas',
    NULL,
    NULL,
    33,
    'Acusado de homicidio'
);

-- Suspeitos
INSERT INTO suspeito (individuo, alcunha) VALUES (2, 'Zézin');
INSERT INTO suspeito (individuo, alcunha) VALUES (3, 'O bagre');

-- Vitimas
INSERT INTO vitima (individuo) VALUES (1);

-- Participacao individuo ocorrencia
INSERT INTO participacao_individuo_ocorrencia (individuo, ocorrencia, descricao) VALUES (
    1,
    '26172839',
    'Assaltado'
);
INSERT INTO participacao_individuo_ocorrencia (individuo, ocorrencia, descricao) VALUES (
    3,
    '26172839',
    'Assassino'
);
INSERT INTO participacao_individuo_ocorrencia (individuo, ocorrencia, descricao) VALUES (
    2,
    '26489213',
    'Testemunha'
);

-- Tipos de careacteristicas
INSERT INTO tipo_caracteristica (nome, descricao) VALUES ('tatuagem', 'Tatuagens de qualquer nartureza');
INSERT INTO tipo_caracteristica (nome, descricao) VALUES ('cicatriz', 'Machucados com marcas permanentes');
INSERT INTO tipo_caracteristica (nome, descricao) VALUES ('aleijamento', 'Alsência de partes do corpo');
INSERT INTO tipo_caracteristica (nome, descricao) VALUES ('gagueira', NULL);

-- Reconhecimento caracteristica
INSERT INTO reconhecimento_caracteristica (suspeito, caracteristica, descricao)
VALUES (2, 'tatuagem', 'tribal no braço esquerdo');
INSERT INTO reconhecimento_caracteristica (suspeito, caracteristica, descricao)
VALUES (2, 'aleijamento', 'Não possui dedo mínimo da mão esquerda');
INSERT INTO reconhecimento_caracteristica (suspeito, caracteristica, descricao)
VALUES (3, 'gagueira', NULL);

-- Organizacao criminosa
INSERT INTO organizacao_criminosa (id, nome, descricao) VALUES (
    1,
    'Gangue dos bandidos',
    'Assaltantes em série'
);

-- Vinculo Criminoso
INSERT INTO vinculo_criminoso (individuo, organizacao) VALUES (2, 1);
INSERT INTO vinculo_criminoso (individuo, organizacao) VALUES (3, 1);

-- Participacao de organizacao criminosa em ocorrencia
INSERT INTO participacao_organizacao_ocorrencia (organizacao, ocorrencia) VALUES (1, '26489213');

-- Agente
INSERT INTO agente (codigo, nome, cpf, rg, cargo, dp) VALUES (
    '001A',
    'Antônio Ferreira',
    '11122233344',
    '12332423',
    'Detetive',
    'PM 14B'
);
INSERT INTO agente (codigo, nome, cpf, rg, cargo, dp) VALUES (
    '003D',
    'Cássio, Andrade',
    '33311155500',
    '32423123',
    'Oficial de Campo',
    'PCMG 28AF'
);

-- Relato de ocorrencia por um agente
INSERT INTO relato_ocorrencia_agente (ocorrencia, agente) VALUES ('26172839', '001A');

-- Presidio
INSERT INTO presidio (numero, nome, tipo, local) VALUES (
    'CB003',
    'Caxambu',
    'Seguranca minima',
    2
);
INSERT INTO presidio (numero, nome, tipo, local) VALUES (
    'PR012',
    'Itariqui',
    'Segurança máxima',
    3
);

-- Inquerito
INSERT INTO inquerito (numero, descricao, data_inicio, data_termino, ocorrencia) VALUES (
    '0001',
    NULL,
    DATE '2017-01-05' + TIME '11:17',
    NULL,
    '26172839'
);
INSERT INTO inquerito (numero, descricao, data_inicio, data_termino, ocorrencia) VALUES (
    '0002',
    NULL,
    DATE '2016-10-03' + TIME '13:17',
    NULL,
    '26489213'
);

-- Instauracao inquerito
INSERT INTO instauracao_inquerito (ocorrencia, inquerito) VALUES ('26172839', '0001');
INSERT INTO instauracao_inquerito (ocorrencia, inquerito) VALUES ('26489213', '0002');

-- Investigador inquerito
INSERT INTO investigador_inquerito (agente, inquerito) VALUES ('001A', '0002');
INSERT INTO investigador_inquerito (agente, inquerito) VALUES ('001A', '0001');

-- Acao penal
INSERT INTO acao_penal (numero, descricao, data_inicio, data_termino, inquerito, presidio, reu) VALUES (
    '4532',
    'Julgado pelo juiz de algum lugar',
    DATE '2017-02-03',
    DATE '2017-05-03',
    '0001',
    'CB003',
    2
);

-- Relato de ocorrencia por vitima
INSERT INTO relato_ocorrencia_vitima (vitima, ocorrencia, data) VALUES (
    1,
    '26489213',
    DATE '2016-09-28' + TIME '16:30'
);

-- Tipos de objeto
INSERT INTO tipo_objeto (nome, descricao) VALUES (
    'Arma de fogo',
    'Artefato que lança um ou mais projéteis em alta velocidade através de uma explosão.'
);
INSERT INTO tipo_objeto (nome, descricao) VALUES (
    'Faca',
    'Instrumento constituído por lâmina cortante presa a um cabo; cuchila.'
);
INSERT INTO tipo_objeto (nome, descricao) VALUES (
    'Automovel',
    'Veículo que se locomove sobre rodas, para transporte de passageiros ou de cargas.'
);
INSERT INTO tipo_objeto (nome, descricao) VALUES (
    'Aparelho eletrônico',
    'Celulares, computadores, tablets...'
);

-- Objetos
INSERT INTO objeto (id, nome, tipo, descricao) VALUES (
    1,
    'Cutelo',
    'Faca',
    'Encontrado no local do crime'
);
INSERT INTO objeto (id, nome, tipo, descricao) VALUES (
    2,
    'Carro',
    'Automovel',
    'Siena preto; placa HFT-3344'
);
INSERT INTO objeto (id, nome, tipo, descricao) VALUES (
    3,
    'Celular',
    'Aparelho eletrônico',
    'Celular Motorola preto; aprox. 5"; tela trincada'
);

-- Licacao de um objeto a uma ocorrencia
INSERT INTO ligacao_ocorrencia_objeto (objeto, ocorrencia, tipo) VALUES (1, '26489213', 'Artefato do criminoso');
INSERT INTO ligacao_ocorrencia_objeto (objeto, ocorrencia, tipo) VALUES (3, '26489213', 'Roubo');
INSERT INTO ligacao_ocorrencia_objeto (objeto, ocorrencia, tipo) VALUES (2, '26172839', 'Encontrado no local do crime');
