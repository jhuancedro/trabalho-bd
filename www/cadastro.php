<!--suppress JSUnusedLocalSymbols -->
<?php
session_start();
?>
<html lang="en">
<head>
    <meta charset="utf-8"/>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <!--<link rel="icon" type="image/png" href="assets/img/favicon.ico">-->
    <title>Cadastro</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet"/>

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet"/>

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet"/>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!--    <script src="js/cadastro_suspeito.js"></script>-->
    <!--    <script src="js/index.js"></script>-->
</head>
<body>
<div class="wrapper">
    <div class="sidebar" data-color="grey">

        <div class="sidebar-wrapper">

            <div class="logo">

            </div>
            <ul class="nav">
                <li>
                    <a href="busca.php">
                        <i class="pe-7s-note2"></i>
                        <p>Busca</p>
                    </a>
                </li>
                <li>
                    <a href="cadastro.php">
                        <i class="pe-7s-user"></i>
                        <p>Cadastro</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Cadastro</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="index.php">
                                <p>Sair</p>
                            </a>
                        </li>
                        <li class="separator hidden-lg hidden-md"></li>
                    </ul>
                </div>

            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Cadastrar Suspeito</h4>
                            </div>
                            <div class="content">
                                <form id="form-cadastro" action="insert_suspeito.php" method="post">
                                    <div id="div-form-cadastro">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label>Nome</label>
                                                    <input type="text" name="nome" class="form-control"
                                                           placeholder="Nome" value="">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Alcunha</label>
                                                    <input type="text" name="alcunha" class="form-control"
                                                           placeholder="Alcunha" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>RG</label>
                                                    <input type="text" name="rg" class="form-control" placeholder="RG"
                                                           value="">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>CPF</label>
                                                    <input type="text" name="cpf" class="form-control" placeholder="CPF"
                                                           value="">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Idade</label>
                                                    <input type="number" name="idade" class="form-control"
                                                           placeholder="Idade" value="33">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Descrição</label>
                                                    <input type="text" name="descricao" class="form-control"
                                                           placeholder="Descrição" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <h4 class="text-center text-info">
                                        <?php
                                        if (array_key_exists('update_suspeito_ok', $_SESSION)) {
                                            if ($_SESSION['update_suspeito_ok'] == false)
                                                echo '<text style="color: #cc2127">Algo deu errado! D=</text>';
                                            else
                                                echo '<text style="color: #2fcc4c">Update efetuado com sucesso!</text>';

                                            unset($_SESSION['update_suspeito_ok']);
                                        }
                                        ?>
                                    </h4>
                                    <button type="submit" class="btn btn-info btn-fill pull-right">Cadastrar</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                            <footer>
                            </footer>
                        </div>

                        <div class="card">
                            <div class="header">
                                <h4 class="title">Registrar ocorrência</h4>
                            </div>
                            <div class="content">
                                <form id="form-registro" action="insert_ocorrencia.php" method="post">
                                    <div id="div-form-cadastro">
                                        <div class="row">
<!--                                            <div class="col-md-4">-->
<!--                                                <div class="form-group">-->
<!--                                                    <label>Registro</label>-->
<!--                                                    <input type="text" name="registro" class="form-control"-->
<!--                                                           placeholder="Registro" value="">-->
<!--                                                </div>-->
<!--                                            </div>-->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Data</label>
                                                    <input name="data" class="form-control" type="date">
                                                    <input name="hora" class="form-control" type="time">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>DP</label>
                                                    <select class="form-control dropdown" name="dp" id="sel1">
                                                        <?php
                                                        $conn = pg_connect("dbname=postgres user=postgres password=postgres");

                                                        $resposta = pg_query($conn,
                                                            "SELECT numero FROM suspeitos.departamento_policia;");

                                                        for ($i = 0; $i < pg_num_rows($resposta); $i++) {
                                                            $opcao = pg_fetch_row($resposta)[0];
                                                            echo "<option>$opcao</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Rua</label>
                                                    <input type="text" name="rua" class="form-control" placeholder="Rua">
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Bairro</label>
                                                    <input type="text" name="bairro" class="form-control" placeholder="Bairro">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>Número</label>
                                                    <input type="number"  value="1234" name="numero" class="form-control" placeholder="Número">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Cidade</label>
                                                    <input type="text" name="cidade" class="form-control" placeholder="Cidade">
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Estado</label>
                                                    <input type="text" name="estado" class="form-control" placeholder="Estado">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Descrição</label>
                                                    <input type="text" name="descricao" class="form-control"
                                                           placeholder="Descrição" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <h4 class="text-center text-info">
                                        <?php
                                        if (array_key_exists('update_ocorrencia_ok', $_SESSION)) {
                                            if ($_SESSION['update_ocorrencia_ok'] == false)
                                                echo '<text style="color: #cc2127">Algo deu errado! D=</text>';
                                            else
                                                echo '<text style="color: #2fcc4c">Update efetuado com sucesso!</text>';

                                            unset($_SESSION['update_ocorrencia_ok']);
                                        }
                                        ?>
                                    </h4>
                                    <button type="submit" class="btn btn-info btn-fill pull-right">Registrar</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                            <footer>
                            </footer>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>
                        <li>

                        </li>
                    </ul>
                </nav>
                <p class="copyright pull-right">

                </p>
            </div>
        </footer>

    </div>
</div>


</body>

<!--   Core JS Files   -->
<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>-->

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>

</html>
