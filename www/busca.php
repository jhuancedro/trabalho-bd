<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <!--<link rel="icon" type="image/png" href="assets/img/favicon.ico">-->
    <title>Sistema</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script src="js/busca.js"></script>
    <script src="js/index.js"></script>
</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="grey" >
        <div class="sidebar-wrapper">
            <div class="logo">

            </div>

            <ul class="nav">
                <li>
                    <a href="busca.html">
                        <i class="pe-7s-note2"></i>
                        <p>Busca</p>
                    </a>
                </li>
                <li>
                    <a href="cadastro.php">
                        <i class="pe-7s-user"></i>
                        <p>Cadastro</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Busca</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">


                        <li>
                            <a href="">
                                <i class="fa fa-search"></i>
                                <p class="hidden-lg hidden-md">Buscar</p>
                            </a>
                        </li>

                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="index.php">
                                <p>Sair</p>
                            </a>
                        </li>
                        <li class="separator hidden-lg hidden-md"></li>
                    </ul>


                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Resultados da busca</h4>
                            </div>
                            <div class="content">
                                <div class="row" id="resultados">
                                    <!--
                                    <div class="col-md-4">
                                        <div class="card card-user">
                                            <div>
                                                <img src="assets/img/faces/perfil.jpg" width="100%" height="100%"/>
                                            </div>
                                            <br>
                                            <div>
                                                <h6>Nome:<br>Nome Exemplo</h6>
                                                <br>
                                                <h6>Alcunha:<br>Alcunha Exemplo</h6>
                                                <br>
                                                <h6>Identificação:<br>Identificação Exemplo</h6>
                                            </div>
                                            <br>
                                            <hr>
                                            <div class="text-center">
                                                <a href="ficha.html">
                                                    <h6 class="title">Ver ficha completa</h6>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card card-user">
                                            <div>
                                                <img src="assets/img/faces/perfil.jpg" width="100%" height="100%"/>
                                            </div>
                                            <br>
                                            <div>
                                                <h6>Nome:<br>Nome Exemplo</h6>
                                                <br>
                                                <h6>Alcunha:<br>Alcunha Exemplo</h6>
                                                <br>
                                                <h6>Identificação:<br>Identificação Exemplo</h6>
                                            </div>
                                            <br>
                                            <hr>
                                            <div class="text-center">
                                                <a href="ficha.html">
                                                    <h6 class="title">Ver ficha completa</h6>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="card card-user">
                                            <div>
                                                <img src="assets/img/faces/perfil.jpg" width="100%" height="100%"/>
                                            </div>
                                            <br>
                                            <div>
                                                <h6>Nome:<br>Nome Exemplo</h6>
                                                <br>
                                                <h6>Alcunha:<br>Alcunha Exemplo</h6>
                                                <br>
                                                <h6>Identificação:<br>Identificação Exemplo</h6>
                                            </div>
                                            <br>
                                            <hr>
                                            <div class="text-center">
                                                <a href="ficha.html">
                                                    <h6 class="title">Ver ficha completa</h6>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card card-user">
                                            <div>
                                                <img src="assets/img/faces/perfil.jpg" width="100%" height="100%"/>
                                            </div>
                                            <br>
                                            <div>
                                                <h6>Nome:<br>Nome Exemplo</h6>
                                                <br>
                                                <h6>Alcunha:<br>Alcunha Exemplo</h6>
                                                <br>
                                                <h6>Identificação:<br>Identificação Exemplo</h6>
                                            </div>
                                            <br>
                                            <hr>
                                            <div class="text-center">
                                                <a href="ficha.html">
                                                    <h6 class="title">Ver ficha completa</h6>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card card-user">
                                            <div>
                                                <img src="assets/img/faces/perfil.jpg" width="100%" height="100%"/>
                                            </div>
                                            <br>
                                            <div>
                                                <h6>Nome:<br>Nome Exemplo</h6>
                                                <br>
                                                <h6>Alcunha:<br>Alcunha Exemplo</h6>
                                                <br>
                                                <h6>Identificação:<br>Identificação Exemplo</h6>
                                            </div>
                                            <br>
                                            <hr>
                                            <div class="text-center">
                                                <a href="ficha.html">
                                                    <h6 class="title">Ver ficha completa</h6>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <footer class="footer">
                <div class="container-fluid">
                    <nav class="pull-left">
                        <ul>
                            <li>

                            </li>
                        </ul>
                    </nav>
                    <p class="copyright pull-right">

                    </p>
                </div>
            </footer>


        </div>
    </div>


</body>
<!--   Core JS Files   -->
<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>-->

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>


</html>